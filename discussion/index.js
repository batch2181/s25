

// JSON OBJECTS
//  - JSON stands for Javascript Object Notation
//  - A common use of JSON is to read data from a web server, and display the data in a web page
//  - Features of JSON:
//     - It is a lightweight data-interchange format
//     - IT is easy to read and write
//     - It is easy for machines to parse and generate


// JSON Objects
//  - JSON also use the "key/value pairs" just like the object properties in Javascript
//  - Key/property names requires to be enclosed with double quotes. 
/*
	Syntax:
	{
		"propertyA" : "valueA",
		"propertyB" : "valueB"
	}
*/

// {
//     "city": "Quezon City",
//     "province" : "Metro Manila",
//     "country" : "Philippines"
// }

// "cities" : [
//     {
//         "city" : "Quezon City",
//         "province" : "Metro Manila",
//         "country" : "PHilippines"
//     },
//     {
//         "city" : "Manila City",
//         "province" : "Metro Manila",
//         "country" : "PHilippines"
//     },
//     {
//         "city" : "Makati City",
//         "province" : "Metro Manila",
//         "country" : "PHilippines"
//     }
// ]

let batchesArr = [
    {
        batchName: "Batch218",
        schedule: "Part Time",
    },
    {
        batchName: "Batch219",
        schedule: "Full Time"
    }
]

console.log(batchesArr);

console.log("---------------------")
// The "stringify" method is used to convert Javascript Objects into a string
console.log("Result of stringify method:")
console.log(JSON.stringify(batchesArr))

console.log("---------------------")

let data = JSON.stringify({
    name: "John",
    age: 31,
    address: {
        city: "Manila",
        country: "Philippines"
    }
});

console.log(data);

// let firstName = prompt("Enter your first name:")
// let lastName = prompt("Enter your last name:")
// let email = prompt("Enter your email:")
// let password = prompt("Enter your password:")

// let otherData = JSON.stringify({
//     firstName: firstName,
//     lastName: lastName,
//     email: email,
//     password: password
// })

// console.log(otherData);

let batchesJSON = `[
    {
        "batchName" : "Batch 218",
        "schedule": "Part Time"
    },
    {
        "batchName" : "Batch 219",
        "schedule": "Full Time"
    }
]`
console.log(batchesJSON)
console.log(JSON.parse(batchesJSON));

let parseBatches = JSON.parse(batchesJSON)
console.log(parseBatches)
console.log(parseBatches[0].batchName)

let stringifiedObject = `{
    "name": "John",
    "age": 31,
    "address": {
        "city": "Manila",
        "country": "Philippines"
    }
}`

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject))